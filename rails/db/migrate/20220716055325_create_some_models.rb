class CreateSomeModels < ActiveRecord::Migration[7.0]
  def change
    create_table :some_models do |t|
      t.text :some_string
      t.integer :some_integer

      t.timestamps
    end
  end
end

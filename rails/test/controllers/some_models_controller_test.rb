require "test_helper"

class SomeModelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @some_model = some_models(:one)
  end

  test "should get index" do
    get some_models_url, as: :json
    assert_response :success
  end

  test "should create some_model" do
    assert_difference("SomeModel.count") do
      post some_models_url, params: { some_model: { some_integer: @some_model.some_integer, some_string: @some_model.some_string } }, as: :json
    end

    assert_response :created
  end

  test "should show some_model" do
    get some_model_url(@some_model), as: :json
    assert_response :success
  end

  test "should update some_model" do
    patch some_model_url(@some_model), params: { some_model: { some_integer: @some_model.some_integer, some_string: @some_model.some_string } }, as: :json
    assert_response :success
  end

  test "should destroy some_model" do
    assert_difference("SomeModel.count", -1) do
      delete some_model_url(@some_model), as: :json
    end

    assert_response :no_content
  end
end

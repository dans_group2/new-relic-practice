class SomeModelsController < ApplicationController
  before_action :set_some_model, only: %i[ show update destroy ]

  # GET /some_models
  def index
    @some_models = SomeModel.all

    render json: @some_models
  end

  # GET /some_models/1
  def show
    render json: @some_model
  end

  # POST /some_models
  def create
    @some_model = SomeModel.new(some_model_params)

    if @some_model.save
      render json: @some_model, status: :created, location: @some_model
    else
      render json: @some_model.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /some_models/1
  def update
    if @some_model.update(some_model_params)
      render json: @some_model
    else
      render json: @some_model.errors, status: :unprocessable_entity
    end
  end

  # DELETE /some_models/1
  def destroy
    @some_model.destroy
  end

  def use_cache
    params.permit(:id)
    id = params[:id]

    some_model = Rails.cache.fetch("some_model_#{id}", expires_in: 30.seconds) do
      sleep(3.seconds)
      SomeModel.find(id)
    end

    render json: some_model
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_some_model
      @some_model = SomeModel.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def some_model_params
      params.require(:some_model).permit(:some_string, :some_integer)
    end
end

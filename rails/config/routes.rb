Rails.application.routes.draw do
  resources :some_models
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root 'some_models#index'

  resources :some_models
  post 'some_models/use_cache' => 'some_models#use_cache'
end

import type {NextPage} from 'next'
import styles from '../styles/Home.module.css'
import {useEffect, useState} from "react";
import {getAllSomeModels, cacheSomeModel} from "../endpoints/calls";
import type {SomeModel} from '../types/SomeModel.d'

const Home: NextPage = () => {
    const [someModels, setSomeModels] = useState<SomeModel[]>([]);
    const [cachedModel, setCachedModel] = useState<SomeModel>();
    const [cacheModelId, setCacheModelId] = useState<number>(1);

    useEffect(() => {
        getAllSomeModels().then((data) => setSomeModels(data))
    }, []);

    const handleCacheButton = async (id: number) => {
        const someModel = await cacheSomeModel(id);
        setCachedModel(someModel)
    }



    return (
        <div className={styles.container}>

            <main className={styles.main}>
                <input type="number" value={cacheModelId} onChange={(e) => setCacheModelId(Number(e.target.value))} />
                <button onClick={() => handleCacheButton(cacheModelId)}>
                    Call Cache Model endpoint
                </button>

                <div>
                    {JSON.stringify(cachedModel)}
                </div>
                <ul>
                    <h1>Some Models from Backend</h1>
                    {someModels.map(e => <li>{JSON.stringify(e)}</li>)}
                </ul>
            </main>
        </div>
    )
}

export default Home

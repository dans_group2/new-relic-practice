
type Config = {
    hostName: string
}

function endpointConfig() : Config  {
    const env = process.env.NODE_ENV;

    const commonConfig = {};

    if (env == 'development') {
        return {
            ...commonConfig,
            hostName: 'http://localhost:3001'
        }
    } else {
        // production
        return {
            ...commonConfig,
            hostName: 'http://learn-new-relic-prod.eba-tueq3njd.us-west-1.elasticbeanstalk.com'
        }
    }
}


export { endpointConfig }
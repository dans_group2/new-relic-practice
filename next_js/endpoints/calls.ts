import {endpointConfig} from "./config";
import type {SomeModel} from '../types/SomeModel'

const hostName = endpointConfig()['hostName']

const commonFetchOptions : RequestInit = {
    headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-type": "application/json"

}

}

export async function getAllSomeModels() : Promise<SomeModel[]>{
    const res : Response = await fetch(hostName + '/some_models', {...commonFetchOptions, method: 'GET'} );
    if (res.status == 200) {
        const data = res.json();
        return data;
    } else {
        const message = 'getAllSomeModels failed';
        console.error(message);
        throw message;
    }
}

export async function cacheSomeModel(id: number): Promise<SomeModel> {
    const res : Response = await fetch(hostName + '/some_models/use_cache', {...commonFetchOptions, method: 'POST', body: JSON.stringify({id})})

    if (res.status == 200) {
        const data = res.json()
        return data;
    } else {
        const message = 'cacheSomeModel failed';
        console.error(message)
        throw message;
    }
}
export type SomeModel = {
    id: number,
    some_string: string,
    some_integer: number,
    created_at: Date,
    updated_at: Date
}